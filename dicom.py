import pydicom 
from pydicom import dcmread
import matplotlib.pyplot as plt
from cv2 import cv2
from PIL import Image
import os
import pandas as pd
import time

def return_transform_img(ds, window_center, window_width):
    img = ds.pixel_array
    ds.pixel_array
    print(img.shape)
    intercept = ds[('0028','1052')].value
    slope = ds[('0028','1053')].value

    img = (img*slope +intercept)
    img_min = window_center - window_width//2
    img_max = window_center + window_width//2
    img[img<img_min] = img_min
    img[img>img_max] = img_max

    # img[img<img_min] = 0
    # img[img>img_max] = 255

    img = (img - img_min) / (img_max - img_min)

    return img

train_folder = 'train'

dicom_names = pd.read_csv('train_dicom_names.csv').values

if not os.path.exists(train_folder):
    os.mkdir(train_folder)


for name in dicom_names:
    print(name[0])
    time.sleep(2.500)
    os.system('kaggle competitions download -c rsna-intracranial-hemorrhage-detection -f rsna-intracranial-hemorrhage-detection/stage_2_train/' + str(name[0]) + '.dcm')
    # kaggle competitions download -c rsna-intracranial-hemorrhage-detection -f rsna-intracranial-hemorrhage-detection/stage_2_train/ID_0000f1657.dcm

    fileName = name[0]
    ds = pydicom.read_file(fileName + ".dcm") # read dicom image

    img_subdural = return_transform_img(ds, 80, 200)
    img_brain = return_transform_img(ds, 40, 80)
    img_bone = return_transform_img(ds, 600, 2800)

    cmap = plt.cm.gray

    if not os.path.exists(train_folder + '/' +fileName):
        os.mkdir(train_folder + '/' +fileName)

    # norm = plt.Normalize(vmin=data.min(), vmax=data.max())
    image = cmap(img_subdural)
    print(image)
    print(image.shape)
    plt.imsave('test.jpg', image)
    img_pil = Image.open('test.jpg')
    img = img_pil.resize((224,224), Image.ANTIALIAS)
    img.save(train_folder + '/' +fileName + '/subdural.jpg')

    image = cmap(img_brain)
    plt.imsave('test.jpg', image)
    img_pil = Image.open('test.jpg')
    img = img_pil.resize((224,224), Image.ANTIALIAS)
    img.save(train_folder + '/' + fileName + '/brain.jpg')

    image = cmap(img_bone)
    plt.imsave('test.jpg', image)
    img_pil = Image.open('test.jpg')
    img = img_pil.resize((224,224), Image.ANTIALIAS)
    img.save(train_folder + '/' +fileName + '/bone.jpg')

    # os.system('rm ' + fileName + '.dcm')
    
# im = plt.imshow(img, cmap='gray')

# # plt.imshow(img, cmap='gray', vmin=0, vmax=255)
# plt.axis('off')
# im.axes.get_xaxis().set_visible(False)
# im.axes.get_yaxis().set_visible(False)
# # plt.savefig('res.png')
# plt.savefig('res.png', bbox_inches='tight', pad_inches = 0)
# plt.show()

# # print(ds)