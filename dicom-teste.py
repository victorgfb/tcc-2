import pydicom 
from pydicom import dcmread
import matplotlib.pyplot as plt
from cv2 import cv2
from PIL import Image
import numpy as np

def return_transform_img(ds, window_center, window_width):
    img = ds.pixel_array
    ds.pixel_array
    print(img.shape)
    intercept = ds[('0028','1052')].value
    slope = ds[('0028','1053')].value

    img = (img*slope +intercept)
    img_min = window_center - window_width//2
    img_max = window_center + window_width//2
    img[img<img_min] = img_min
    img[img>img_max] = img_max

    # img[img<img_min] = 0
    # img[img>img_max] = 255

    img = (img - img_min) / (img_max - img_min)

    return img

ds = pydicom.read_file("ID_12cadc6af.dcm") # read dicom image

img = return_transform_img(ds, 40, 80)

cmap = plt.cm.gray

# norm = plt.Normalize(vmin=data.min(), vmax=data.max())
image = cmap(img)

# save the image
plt.imsave('test.jpg', image)


img_pil = Image.open('brain.jpg')
img = img_pil.resize((224,224), Image.ANTIALIAS)
img.save('resized.jpg')

npImage = np.asarray(image)

print(npImage.shape)

im = plt.imshow(img)

# plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.axis('off')
im.axes.get_xaxis().set_visible(False)
im.axes.get_yaxis().set_visible(False)
# plt.savefig('res.png')
plt.savefig('res.png', bbox_inches='tight', pad_inches = 0)
plt.show()

# print(ds)